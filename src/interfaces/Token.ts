export interface Token {
    userID: string,
    token: string
}