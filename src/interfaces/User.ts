export interface User {
    id: string,
    fullName: string,
    password: string,
    passwordID: string,
    hashedPassword: string,
    email: string,
    emailVerified: boolean
}