import dotenv from 'dotenv';
import express, { Application, ErrorRequestHandler } from 'express';
import morgan from 'morgan';
import cors from 'cors';

// Environment configurations
dotenv.config()

import { initiatePassport } from './middlewares/passport';
import { mountRoutes } from './routes';

// Express configuraion
const app: Application = express()

app.use(express.json())
app.use(morgan('tiny'))
app.use(cors())

// Server configuration
const PORT = process.env.PORT || 8000

// Initiate passport and strategies
initiatePassport(app)

// Mount application routes
mountRoutes(app)

// Handle HTTP(S) errors
app.use(((err, req, res, next) => {
    console.log(err);
    res.status(err.status || 500).send({ error: err.message || err })
}) as ErrorRequestHandler)

app.listen(PORT, () => { console.log('Listening...') })