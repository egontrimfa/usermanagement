-- UUID creator module
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

-- Dropping existing tables
DROP TABLE IF EXISTS passwords;
DROP TABLE IF EXISTS tokens;
DROP TABLE IF EXISTS users;

-- Auto update timestamp function
CREATE OR REPLACE FUNCTION trigger_set_update()
RETURNS TRIGGER AS $$
BEGIN
    NEW.updated_at = NOW();
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

-- Users table
CREATE TABLE users(
    id UUID DEFAULT uuid_generate_v4(),
    full_name VARCHAR(255) NOT NULL,
    email VARCHAR(127) NOT NULL UNIQUE,
    email_verified BOOLEAN NOT NULL,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    PRIMARY KEY(id)
);

-- Default values for the users table 
ALTER TABLE users ALTER COLUMN email_verified SET DEFAULT FALSE;

-- Implement auto update function on users
CREATE TRIGGER set_users_update
BEFORE UPDATE ON users
FOR EACH ROW
EXECUTE PROCEDURE trigger_set_update();

-- Passwords table
CREATE TABLE passwords(
    id SERIAL,
    user_id UUID,
    hashed_password VARCHAR(256) NOT NULL,
    reset_in_progress BOOLEAN NOT NULL,
    reset_code VARCHAR(256) DEFAULT NULL UNIQUE, -- PostgreSQL treats NULL as distinct value
    reset_expires TIMESTAMPTZ DEFAULT NULL,
    active BOOLEAN NOT NULL,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    PRIMARY KEY(id),
    CONSTRAINT fk_user_password
        FOREIGN KEY(user_id)
            REFERENCES users(id)
            ON DELETE CASCADE
);

-- Default values for the password table 
ALTER TABLE passwords ALTER COLUMN reset_in_progress SET DEFAULT FALSE;
ALTER TABLE passwords ALTER COLUMN active SET DEFAULT TRUE;

-- Implement auto update function on users
CREATE TRIGGER set_passwords_update
BEFORE UPDATE ON passwords
FOR EACH ROW
EXECUTE PROCEDURE trigger_set_update();

-- Verification tokens table
CREATE TABLE tokens(
    id SERIAL,
    user_id UUID,
    token VARCHAR(128) NOT NULL UNIQUE,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    PRIMARY KEY(id),
    CONSTRAINT fk_user_token
        FOREIGN KEY(user_id)
            REFERENCES users(id)
            ON DELETE CASCADE
);