import { QueryResult } from 'pg';
import { query } from './index';

export const getUsers = async (): Promise<QueryResult<any>> => {
    return query('SELECT * FROM users')
}

export const signup = async (fullName: string, email: string, hashedPassword: string): Promise<QueryResult<any>> => {
    try {
        await query('BEGIN')

        const newUser = await query('INSERT INTO users(full_name, email) VALUES($1, $2) RETURNING id', [fullName, email]) 
        await query('INSERT INTO passwords(user_id, hashed_password) VALUES($1, $2)', [newUser.rows[0].id, hashedPassword])
        
        await query('COMMIT')

        return query('SELECT id, full_name, email FROM users WHERE id = $1', [newUser.rows[0].id])
    } catch (error) {
        await query('ROLLBACK')
        throw error
    }
}

export const getUserByID = async (id: string): Promise<QueryResult<any>> => {
    return query(`
        SELECT id, full_name, email, email_verified
        FROM users
        WHERE users.id = $1
    `, [id])
}

export const getUserByEmail = async (email: string): Promise<QueryResult<any>> => {
    return query(`
        SELECT users.id, users.full_name, users.email, users.email_verified, passwords.hashed_password
        FROM users
        INNER JOIN passwords ON users.id = passwords.user_id
        WHERE users.email = $1 AND passwords.active = TRUE
        ORDER BY passwords.created_at DESC
        LIMIT 1
    `, [email])
}

export const getUserByResetCode = async (resetToken: string): Promise<QueryResult<any>> => {
    return query(`
        SELECT users.id as user_id, users.full_name, users.email, passwords.id as password_id
        FROM users
        INNER JOIN passwords ON users.id = passwords.user_id
        WHERE passwords.reset_code = $1 AND passwords.reset_expires > NOW()
        LIMIT 1
    `, [resetToken])
}

export const updatePasswordResetAttributes = async (userID: string, resetToken: string): Promise<QueryResult<any>> => {
    return query(`
        UPDATE passwords
        SET reset_in_progress = TRUE,
            reset_code = $1,
            reset_expires = ( NOW() + interval '1h' )
        WHERE id = (
            SELECT id
            FROM passwords
            WHERE user_id = $2
            LIMIT 1
        )
    `, [resetToken, userID])
}

export const resetPassword = async (userID: string, passwordID: string, hashedPassword: string): Promise<QueryResult<any>> => {
    try {
        await query('BEGIN')

        await query(`
            UPDATE passwords
            SET reset_in_progress = FALSE,
                reset_code = NULL,
                reset_expires = NULL,
                active = FALSE
            WHERE id = $1
        `, [passwordID])

        await query(`
            INSERT INTO passwords(user_id, hashed_password)
            VALUES($1, $2)
        `, [userID, hashedPassword])

        await query(`
            UPDATE users
            SET email_verified = TRUE
            WHERE id = $1
        `, [userID])
    
        return query('COMMIT')
    } catch (error) {
        await query('ROLLBACK')
        throw error
    }
}

export const updateUsersEmailVerifiedByID = async (id: string, emailVerified: boolean): Promise<QueryResult<any>> => {
    return query(`
        UPDATE users
        SET email_verified = $1
        WHERE id = $2
    `, [emailVerified ? 'TRUE' : 'FALSE', id])
}

export const createVerificationToken = (userID: string, token: string): Promise<QueryResult<any>> => {
    return query('INSERT INTO tokens(user_id, token) VALUES($1, $2)', [userID, token])
}

export const getTokenByToken = (token: string): Promise<QueryResult<any>> => {
    return query('SELECT user_id, token FROM tokens WHERE token = $1 LIMIT 1', [token])
}