import { Client, ClientConfig } from 'pg';

const { DB_PROTOCOL, DB_USERNAME, DB_PASSWORD, DB_HOST, DB_NAME } = process.env

// PostgreSQL connection configuration
const client = new Client({
    connectionString: `${DB_PROTOCOL}://${DB_USERNAME}:${DB_PASSWORD}@${DB_HOST}/${DB_NAME}`
} as ClientConfig)

const connect = async (): Promise<void> => {
    try {
        await client.connect()
        console.log('Successfully connected to the UM DB (PostgeSQL)');
    } catch (error) {
        await client.end()
    }
}

connect()

export const query = (text: string, params?: any) => client.query(text, params)