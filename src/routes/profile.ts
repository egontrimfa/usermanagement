import { Router } from 'express';

export const router: Router = new (Router as any)()

router.get('/', (req, res, next) => {
    res.send({
        user: req.user
    })
})