import { Application } from 'express';
import passport from 'passport';
import { router as profileRouter } from './profile';
import { router as usersRouter } from './users';

export const mountRoutes = (app: Application) => {
    // Common routes
    app.use('/users', usersRouter)

    // Secure routes
    app.use('/authorize', passport.authenticate('jwt', { session: false}), profileRouter)
}