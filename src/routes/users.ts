import { Router as ExpressRouter } from 'express';
import Router from 'express-promise-router';
import passport, { use } from 'passport';
import jwt from 'jsonwebtoken';

import { getUsers } from '../db/queries';
import { User } from '../interfaces/User';
import { sendVerificationEmail, verifyEmail } from '../services/email';
import { recover, reset } from '../services/password';

export const router: ExpressRouter = new (Router as any)()

router.get('/', async (req, res, next) => {
    const { rows } = await getUsers()
    return res.send(rows)
})

router.post('/signup', async (req, res, next) => {
    passport.authenticate('signup', { session: false }, async (err, user: User, info) => {
        try {
            if (err || !user) return next(err)

            await sendVerificationEmail(user)
    
            return res.send({
                message: 'Signup successful! A verification email has been sent to ' + user.email + '.'
            })
        } catch (error) {
            return next(error)
        }
    })(req, res, next)
})

router.post('/login', async (req, res, next) => {
    passport.authenticate('login', async (err, user, info) => {
        try {
            if (err || !user) return next(err)

            req.login(user, { session: false }, async (error) => {
                if (error) return next(error)

                const accessToken = jwt.sign({ user }, 'TOP_SECRET')
                return res.send({ accessToken })
            })
        } catch (error) {
            return next(error)
        }
    })(req, res, next)
})

router.get('/verify/:token', async (req, res, next) => {
    try {
        await verifyEmail(req.params.token as string)

        return res.send({
            message: 'Your account has been verified. You can log in now!'
        })
    } catch (error) {
        return next(error)
    }
})

router.post('/recover', async (req, res, next) => {
   try {
       const { email } = req.body
       await recover(email)

       return res.send({
           message: 'An email has been sent to ' + email + ' to reset your password.'
       })
   } catch (error) {
       return next(error)
   } 
})

router.post('/reset', async (req, res, next) => {
    try {
        const { token, password } = req.body
        await reset(token, password)

        return res.send({
            message: 'Your password has benn successfully updated.'
        })
    } catch (error) {
        return next(error)
    }
})