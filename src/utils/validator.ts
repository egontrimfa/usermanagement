import bcrypt from 'bcrypt';

export const isValidPassword = (passwordToValidate: string, storedPassword: string): boolean => {
    return bcrypt.compareSync(passwordToValidate, storedPassword)
}