import bcrypt from 'bcrypt';
import { Application } from 'express';
import HttpError from 'http-errors';
import passport from 'passport';
import { ExtractJwt, Strategy as JWTStrategy } from 'passport-jwt';
import { IStrategyOptionsWithRequest, Strategy as LocalStrategy } from 'passport-local';

import { getUserByEmail, signup } from '../db/queries';
import { User } from '../interfaces/User';
import { isValidPassword } from '../utils/validator';

const options: IStrategyOptionsWithRequest = {
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true
}

const signupStrategy = new LocalStrategy(
    options,
    async (req, email, password, done) => {
        try {
            const { rows } = await signup(req.body.fullName, email, bcrypt.hashSync(password, 10))

            if (!rows || rows.length !== 1) {
                return done({ message: 'User could not be created!' })
            }
            
            const rawUser = rows[0]
            const user: User = { id: rawUser.id, fullName: rawUser.full_name, email: rawUser.email } as User

            return done(null, user)
        } catch (error) {
            return done(error)
        }
    }
)

const loginStrategy = new LocalStrategy(
    options,
    async (req, email, password, done) => {
        try {
            const { rows } = await getUserByEmail(email)

            if (!rows || rows.length !== 1) {
                return done({ status: 404, message: 'User not found with the given email address!' })
            }

            const rawUser = rows[0]
            const user: User = { email: rawUser.email, emailVerified: rawUser.email_verified, fullName: rawUser.full_name } as User

            if (!isValidPassword(password, rawUser.hashed_password)) {
                return done({ status: 401, message: 'Wrong Password!' })
            }

            if (!user.emailVerified) {
                return done(HttpError(401, 'Your account has not been verified yet. Please check your email.'))
            }

            return done(null, user)
        } catch (error) {
            return done(error)
        }
    }
)

const tokenVerifierStrategy = new JWTStrategy(
    {
        secretOrKey: 'TOP_SECRET',
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken()
    },
    async (accessToken, done) => {
        try {
            return done(null, accessToken.user)
        } catch (error) {
            done(error)
        }
    }
)

export const initiatePassport = (app: Application) => {
    app.use(passport.initialize())
    passport.use('signup', signupStrategy)
    passport.use('login', loginStrategy)
    passport.use(tokenVerifierStrategy)
}