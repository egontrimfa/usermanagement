import { getUserByResetCode, getUserByEmail, updatePasswordResetAttributes, resetPassword } from "../db/queries"
import HttpError from 'http-errors';
import { generateToken } from "../utils/token";
import { User } from "../interfaces/User";
import { sendEmail } from "../utils/email";
import { MailDataRequired } from "@sendgrid/mail";
import bcrypt from 'bcrypt';

/**
 * @route POST users/recover
 * @desc Recover Password - Generates token, updates passwords and Send password reset email
 */
export const recover = async (email: string) => {
    try {
        const userResult = await getUserByEmail(email)
        if (!userResult.rows || userResult.rows.length !== 1) throw HttpError(401, 'The provided email address is not associated with any user.')

        const rawUser = userResult.rows[0]
        const user: User = { id: rawUser.id, email: rawUser.email, fullName: rawUser.full_name } as User

        const resetToken = generateToken()
        await updatePasswordResetAttributes(user.id, resetToken)

        const subject = 'Password reset request'
        const to = user.email
        const from = 'sapientia.egontrimfa@gmail.com'
        const link = 'http://localhost/#/reset?reset-token=' + resetToken
        const html = `
            <p>Hi ${user.fullName}!</p>
            <p>In order to reset your password on E-Commerce, please click on the following <a href="${link}">link</a>.</p>
            <p>If for some reason the link cannot be clicked, please follow this direct link ${link}.</p>
            <p>If you did not request this, please ignore this email and your password will remain unchanged.</p>
        `

        await sendEmail({ to, from, subject, html } as MailDataRequired)
    } catch (error) {
      throw(error)
    }
}

export const reset = async (resetToken: string, newPassword: string) => {
    try {
        const userResult = await getUserByResetCode(resetToken)
        if (!userResult.rows || userResult.rows.length !== 1) throw HttpError(401, 'Password reset token is invalid or it has expired.')

        const rawUser = userResult.rows[0]
        const user = { id: rawUser.user_id, fullName: rawUser.full_name, email: rawUser.email, passwordID: rawUser.password_id, hashedPassword: bcrypt.hashSync(newPassword, 10) } as User

        await resetPassword(user.id, user.passwordID, user.hashedPassword)

        const subject = 'Your password has benn changed'
        const to = user.email
        const from = 'sapientia.egontrimfa@gmail.com'
        const html = `
            <p>Hi ${user.fullName}!</p>
            <p>This is a confirmation that the password for your account has just been changed.</p>
        `

        await sendEmail({ to, from, subject, html } as MailDataRequired)
    } catch (error) {
        throw(error)
    }
}