import { MailDataRequired } from '@sendgrid/mail';
import { createVerificationToken, getTokenByToken, getUserByID, updateUsersEmailVerifiedByID } from '../db/queries';
import { User } from '../interfaces/User';
import { sendEmail } from '../utils/email';
import { generateToken } from '../utils/token';
import HttpError from 'http-errors';
import { Token } from '../interfaces/Token';

export const sendVerificationEmail = async (user: User) => {
    try {
        const token = generateToken()
        await createVerificationToken(user.id, token)

        const subject = 'Account verification token'
        const to = user.email
        let from = 'sapientia.egontrimfa@gmail.com'
        let link = 'http://localhost/#/login?verification-token=' + token
        let html = `
            <p>Hi ${user.fullName}!</p>
            <p>Thank you for registrating to our E-Commerce platform. In order to verify your identity, please click on the following <a href="${link}">link</a>.</p>
            <p>If for some reason the link cannot be clicked, please follow this direct link ${link}.</p>
            <p>If you did not request this, please ignore this email.</p>
        `

        await sendEmail({ to, from, subject, html } as MailDataRequired )
    } catch (error) {
        throw(error)
    }
}

export const verifyEmail = async (tokenParam: string) => {
    try {
        if (!tokenParam) throw HttpError(400, 'Missing verification token!')

        const tokenResult = await getTokenByToken(tokenParam)
        if (!tokenResult.rows || tokenResult.rows.length !==1) throw HttpError(400, 'We were unable to find a valid token.')

        const rawToken = tokenResult.rows[0]
        const token: Token = { userID: rawToken.user_id, token: rawToken.token }
        const userResult = await getUserByID(token.userID)
        if (!userResult.rows || userResult.rows.length !==1) throw HttpError(400, 'We were unable to find a user for the given token.')

        const rawUser = userResult.rows[0]
        const user: User = { id: rawUser.id, fullName: rawUser.full_name, email: rawUser.email, emailVerified: rawUser.email_verified } as User
        if (user.emailVerified) throw HttpError(400, 'This user has already been verified.')

        await updateUsersEmailVerifiedByID(user.id, true)
    } catch (error) {
        throw(error)
    }
}